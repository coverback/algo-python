#!/usr/bin/env python

"""
This program is designed to test implemented sorting algorythms
"""

from sorting.list_gen import random_list

from sorting.bubble_sort import bubble_sort
from sorting.cocktail_sort import cocktail_sort
from sorting.heapsort import heapsort
from sorting.insertion_sort import insertion_sort
from sorting.merge_sort import merge_sort
from sorting.quicksort import quicksort
from sorting.radix_sort import radix_sort
from sorting.selection_sort import selection_sort
from sorting.shell_sort import shell_sort

ITEMS = 500

################ main ##################

source_list = random_list(ITEMS);
reference_list = sorted(source_list)

# create a function map for easier iteration later on
func_map = {}
func_map['bubble_sort'] = bubble_sort
func_map['cocktail_sort'] = cocktail_sort
func_map['heapsort'] = heapsort
func_map['insertion_sort'] = insertion_sort
func_map['merge_sort'] = merge_sort
func_map['quicksort'] = quicksort
func_map['radix_sort'] = radix_sort
func_map['selection_sort'] = selection_sort
func_map['shell_sort'] = shell_sort

faulty = 0
for func_name, sort_func in func_map.iteritems():
    test_list = source_list[:]
    sort_func(test_list)

    if test_list == reference_list:
        print func_name, "performed correctly"
    else:
        print func_name, "failed!"
        faulty += 1

print "\nTesting ended.", faulty, "faulty algorithms."

