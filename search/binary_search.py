#! /usr/bin/python

""" 
This is an implementation of a binary search of an item in the array
using python
"""

# import only randint function to create random integers
from random import randint


def binary_search(array, element):
    """Search for an element in an array using binary search"""
    # left and right boundaries
    L = 0
    R = len(array) - 1
    i = -1

    # we continue unless boundaries overlap (i.e. no match found)
    while L <= R:
        i = L + (R - L) / 2
        if element == array[i]:
            break
        elif element > array[i]:
            L = i + 1
        else:
            R = i - 1

    # we have exited the loop, check if smth was found
    if L <= R:
        return i
    else:
        return -1


def random_list(length):
    """Return a list filled with random numbers of provided length"""
    list = []
    for i in range(length):
        list.append(randint(0, 99))
    list.sort()
    return list


# create a list with random (0 to 99) numbers
arr = random_list(10)
print "Array of ints:"
print arr

print "Unexistant element -5 at", binary_search(arr, -5)
print "Possible unexistant element 50 at", binary_search(arr, 50)
print "Unexistant element 100 at", binary_search(arr, 100)

for i in range(len(arr)):
    print "Found element", arr[i], "under index", binary_search(arr, arr[i])


