#! /usr/bin/python

""" 
This is an implementation of straightforward naive search of needle in
haystack using python
"""


# global vars
needle = "foo"
haystack_start = "foobar here, what about some  foshit?foo"
haystack_mid   = "Hey, what bout some foobarish foshit?foo"
haystack_last  = "Hey, what about some fobarish foshit?foo"

def naive_search(what, where):
    for i in range(len(where) - len(what) + 1):
        # use range here to compare two strings
        if what == where[i:i + len(what)]:
            return i
    return -1

def naive_search_plain(what, where):
    for i in range(len(where) - len(what) + 1):
        found = True
        for j in range(len(what)):
            if where[i+j] != what[j]:
                found = False
                break
        if found:
            return i
    return -1

def print_idx(length):
    # print numbers representing order (1 for -teen numbers etc)
    for i in range(length / 10):
        print repr(i).ljust(9),
    
    # print a newline
    print
    
    # print cardinal indices from 0 to 9 in loop
    output = ""
    for i in range(length):
        output = output + str(i % 10)
    print output

######################## main code ##########################

print "Search for: " + needle

print_idx(len(haystack_start))
print haystack_start
print "Needle was found at position: " + str(naive_search(needle, haystack_start)) + "\n"

print_idx(len(haystack_mid))
print haystack_mid
print "Needle was found at position: " + str(naive_search(needle, haystack_mid)) + "\n"

print_idx(len(haystack_last))
print haystack_last
print "Needle was found at position: " + str(naive_search(needle, haystack_last)) + "\n"

