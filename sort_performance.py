#!/usr/bin/env python

"""
This program is designed to test and compare performance of the implemented sorting algorythms
"""

import timeit
import sys
import time

from sorting.list_gen import random_list
from sorting.list_gen import almost_sorted_list

from sorting.bubble_sort import bubble_sort
from sorting.cocktail_sort import cocktail_sort
from sorting.heapsort import heapsort
from sorting.insertion_sort import insertion_sort
from sorting.merge_sort import merge_sort
from sorting.quicksort import quicksort
from sorting.radix_sort import radix_sort
from sorting.selection_sort import selection_sort
from sorting.shell_sort import shell_sort

ITEMS = 1000
RUNS = 10

############# functions ################

def delete_last_lines(filename, num):
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    w = open(filename, 'w')
    w.writelines([item for item in lines[:-num]])
    w.close()


def write_html_header(f):
    f.write('<html>\n<head>\n')
    f.write('  <title>Sorting performance data</title>\n')
    f.write('  <link rel="stylesheet" href="perf.css" type="text/css" />\n')
    f.write('</head>\n')
    f.write('<body>\n\n')


def write_html_footer(f):
    f.write('\n</body></html>\n')


def write_table_row(f, sort_name, results):
    f.write('  <tr><td scope="row">' + sort_name + '</td>\n')
    for result in results:
        f.write('    <td>' + result + '</td>\n')
    f.write('  </tr>\n')


def write_table_header(f, items, runs):
    f.write('<p>\n')
    f.write('Performance data fetched on: ' + time.asctime() + '<br />\n')
    f.write('Tested arrays contain ' + str(items) + ' elements. Each sort is run ' + str(runs) + ' times.\n')
    f.write('<table>\n')
    f.write('  <thead>\n')
    f.write('    <th>Name</th>\n')
    f.write('    <th>Random</th>\n')
    f.write('    <th>Almost sorted</th>\n')
    f.write('    <th>Sorted</th>\n')
    f.write('    <th>Almost reversed</th>\n')
    f.write('    <th>Reversed</th>\n')
    f.write('  </thead>\n')
    f.write('  <tbody>\n')


def write_table_footer(f):
    f.write('  </tbody>\n</table>\n</p>\n\n')


################ main ##################

is_file = False
filename = ""
f = None

# If a parameter was given, consider that an output filename
if len(sys.argv) == 2 and sys.argv[1] != None:
    is_file = True
    filename = sys.argv[1]
    try:
        f = open(filename, 'r')
    except IOError as e:
        try:
            f = open(filename, 'w')
            write_html_header(f)
            write_html_footer(f)
        except BaseException as ex:
            print ex
            sys.exit(1)
    f.close()
    delete_last_lines(filename, 1)
    f = open(filename, 'a')


# Different lists to work on
source_lists = []
source_lists.append(("Random", random_list(ITEMS)))
source_lists.append(("Almost sorted", almost_sorted_list(ITEMS)))
source_lists.append(("Sorted", sorted(source_lists[0][1][:])))
source_lists.append(("Almost reversed", source_lists[1][1][:]))
source_lists[3][1].reverse()
source_lists.append(("Reversed", source_lists[2][1][:]))
source_lists[4][1].reverse()

# create a function map for easier iteration later on
func_map = {}
func_map['bubble_sort'] = bubble_sort
func_map['cocktail_sort'] = cocktail_sort
func_map['heapsort'] = heapsort
func_map['insertion_sort'] = insertion_sort
func_map['merge_sort'] = merge_sort
func_map['quicksort'] = quicksort
func_map['radix_sort'] = radix_sort
func_map['selection_sort'] = selection_sort
func_map['shell_sort'] = shell_sort

print "Tested arrays contain", ITEMS, "elements. Each sort is run", RUNS, "times.\n"
if is_file:
    write_table_header(f, ITEMS, RUNS)
else:
    print "Sort name\tRandom\t\tAlmost sorted\tSorted\t\tAlmost reversed\tReversed"

# First, run stock sorting
func_name = "Standard sort"
results = []
for source_name, source_list in source_lists:
    test_list = source_list[:]
    results.append('%.6f'%timeit.timeit('test_list.sort()', 'from __main__ import test_list', number = RUNS))
if is_file:
    write_table_row(f, func_name, results)
    print func_name, "processed"
else:
    output = "\t".join(results)
    print func_name + ":\t", output

# Now test all implemented algorithms
for func_name, sort_func in func_map.iteritems():
    results = []
    for source_name, source_list in source_lists:
        test_list = source_list[:]
        results.append('%.6f'%timeit.timeit(func_name + "(test_list)",
                "from __main__ import test_list," + func_name, number = RUNS))
    if is_file:
        write_table_row(f, func_name, results)
        print func_name, "processed"
    else:
        output = "\t".join(results)
        print func_name + ":\t", output

if is_file:
    write_table_footer(f)
    write_html_footer(f)

