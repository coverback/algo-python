#!/usr/bin/env python

"""
Create a random or an almost sorted list with a specified length.
Also provides a basic visualisation function.
"""

from random import randint


def random_list(length):
    """Return a list filled with random numbers of provided length"""
    list = []
    for i in range(length):
        list.append(randint(0, 99))
    return list


def almost_sorted_list(length):
    """Returned almost (with ~10% mixup) sorted list"""
    list = sorted(random_list(length))
    for i in range(0, length / 2, length / 20):
        list[i], list[length - i - 1] = list[length - i - 1], list[i]
    return list


def visualize_list(list):
    """Shows barred approximate representation of the list values"""
    width = 100  # random constant
    height = 20 # random constant, too
    idx = []
    if len(list) <= width:
        idx = range(len(list))
    else:
        idx = range(0, len(list), len(list) / width)

    maximum = max(list)
    values = []
    for i in idx:
        values.append((100 * list[i] / maximum) * height / 100)

    for i in reversed(range(height)):
        string = ""
        for j in range(len(values)):
            if values[j] >= i:
                string += "#"
            else:
                string += " "
        print string

