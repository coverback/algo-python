#! /usr/bin/env python

"""
Radix sort implemented in python
"""

from list_gen import random_list
from math import log

# For decimal system keys are 10
keys = 10

################ functions ##################

def get_max_len(list):
    """Return maximal length of the key in the list"""
    max = list[0]
    for i in range(1, len(list)):
        if list[i] > max:
            max = list[i]
    # Now with the maximal number, get its length
    return int(log(max, keys) + 1)


def radix_sort(list):
    """Radix sort in place"""
    for k in range(get_max_len(list)):
        # Init the basket. Number of baskets must be same as keys
        baskets = [[] for i in range(keys)]

        for i in range(len(list)):
            lsd = (list[i] / (keys ** k)) % keys
            baskets[lsd].append(list[i])
        #print baskets

        # Discard previous original list and replace it with basket's concatenation
        list[:] = []
        for i in baskets:
            list += i


################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list
    print

    radix_sort(list)

    print list


