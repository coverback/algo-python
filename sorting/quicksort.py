#! /usr/bin/env python

"""
Quicksort implemented in python
"""

from list_gen import random_list


################ functions ##################

def quicksort(list, boundaries = ()):
    """Quicksort in place"""
    if boundaries == ():
        boundaries = 0, len(list) - 1
    #print "range:", boundaries

    # Recursion termination when one or less elements left
    if boundaries[0] >= boundaries[-1]:
        return

    # Select the pivot
    pivot_idx = (boundaries[0] + boundaries[-1]) / 2
    pivot = list[pivot_idx]
    #print "Chosen pivot is", pivot, "at index", pivot_idx

    # Exchange items around pivot
    i, j = boundaries[0], boundaries[-1]
    while True:
        while list[i] < pivot:
            i += 1
        while list[j] > pivot:
            j -= 1
        if i >= j:
            break

        if list[i] == list[j]:
            # We have some trouble now. No sense in exchanging two pivot values
            # Same values may not adjacent. We can't just stop here.
            exchanged = False
            for k in range(i + 1, j):
                if list[k] != pivot:
                    # Exchange current element with the next one and hope it will save us
                    list[i], list[k] = list[k], list[i]
                    #print "Exception: Exchanged", i, list[k], "with", k, list[i]#, list
                    exchanged = True
            if exchanged:
                continue
            else:
                # If none elements were exchanged, means there are only pivot value in between
                break

        list[i], list[j] = list[j], list[i]
        #print "Exchanged", i, list[j], "with", j, list[i], list

    #print "After partition", i, j, list[boundaries[0]:boundaries[-1] + 1]

    # Call quicksort recursively for each segment
    quicksort(list, (boundaries[0], i - 1))
    quicksort(list, (j + 1, boundaries[-1]))


################# main #####################

if __name__ == "__main__":
    list = random_list(40)
    print "List to be sorted:"
    print list
    print

    quicksort(list)

    print list


