#! /usr/bin/env python

"""
Shell sort implemented in python
"""

from list_gen import random_list


################ functions ##################

def insertion_sort_gap(list, gap, start = 0):
    """Performs an insertion sorting, talking in account gap between elements"""
    el_range = range(start, len(list), gap)
    #print el_range
    for i, v in enumerate(el_range[1:]):
        #print "Processing v =", v, i, list[v], "Previous elements to be analysed:", el_range[:i + 1]
        #print "Current list", list
        # Put this eleent throughout the preceding list, stopping in position, where it's fit
        j = el_range[i]
        while j >= el_range[0] and list[j] > list[j + gap]:
            #print "Items to compare and exchange", list[j], list[j + gap]
            # Exchange elements, don't forget they're not adjacent but separated with a gap
            list[j], list[j + gap] = list[j + gap], list[j]
            j -= gap
        #print "Item will be left at", j + gap


def shell_sort(list):
    """Shell sort in place"""
    # TODO replace this with decent calculation of gaps and create a loop or smth
    gaps = [4, 2, 1]
    for i in gaps:
        for j in range(i):
            if __name__ == "__main__":
                print "call in", i, j
            insertion_sort_gap(list, i, j)

#    insertion_sort_gap(list, 4, 0);
#    insertion_sort_gap(list, 4, 1);
#    insertion_sort_gap(list, 4, 2);
#    insertion_sort_gap(list, 4, 3);
#    insertion_sort_gap(list, 2, 0);
#    insertion_sort_gap(list, 2, 1);
#    insertion_sort_gap(list, 1);


################# main #####################

if __name__ == "__main__":
    list = random_list(50)
    print "List to be sorted:"
    print list
    print

    shell_sort(list)

    print list


