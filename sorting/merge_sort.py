#! /usr/bin/env python

"""
Merge sort implemented in python
"""

from list_gen import random_list


################ functions ##################

def merge_lists(l1, l2):
    """Merge two sorted list into one and return it"""
    ret_list = []
    while True:
        if len(l1) == 0:
            ret_list.extend(l2)
            break
        if len(l2) == 0:
            ret_list.extend(l1)
            break

        if l1[0] <= l2[0]:
            ret_list.append(l1.pop(0))
        else:
            ret_list.append(l2.pop(0))

    return ret_list

def merge_recursive(list):
    """Function returs sorted list"""
    # Length of 1 means array is sorted, return it
    ln = len(list)
    assert ln != 0
    if ln <= 1:
        return list

    # Split in two, sort them
    return merge_lists(merge_recursive(list[:(ln / 2)]), merge_recursive(list[(ln / 2):]))

def merge_sort(list):
    list[:] = merge_recursive(list)
    return


################# main #####################

if __name__ == "__main__":
    list = random_list(50)
    print "List to be sorted:"
    print list
    print

    merge_sort(list)

    print list


