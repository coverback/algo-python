#! /usr/bin/env python

"""
Bubble sort modification called Cocktail sort
"""

from list_gen import random_list


################ functions ##################

def cocktail_sort(list):
    """Use Cocktail sort to sort a list in place"""

    sorted = False
    elements = range(len(list) - 1)

    while not sorted:
        sorted = True
        elements.reverse()
        for i in elements:
            if list[i] > list[i + 1]:
                # swap these two values then
                list[i], list[i + 1] = list[i + 1], list[i]
                sorted = False


################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list

    cocktail_sort(list)

    print list

