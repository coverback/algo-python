#! /usr/bin/env python

"""
Simple implementation of Bubble sort algorith in Python
"""

from list_gen import random_list

################ functions ##################

def bubble_sort(list):
    """Use Bubble sort to sort a list in place"""
    sorted = False

    while not sorted:
        sorted = True
        for i in range(len(list) - 1):
            if list[i] > list[i + 1]:
                # swap these two values then
                list[i], list[i + 1] = list[i + 1], list[i]
                sorted = False


################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list

    bubble_sort(list)

    print list

