#! /usr/bin/env python

"""
Heapsort implemented in python
"""

from list_gen import random_list
from math import log


################ functions ##################

def display_heap(heap):
    """Display the heap row by row"""
    # Learn maximal width in number of elements
    max_row = int(log(len(heap), 2))
    max_width = 2**max_row
    for i in range(len(heap)):
        row = int(log(i + 1, 2))
        spacing = 4 * max_width / (2**row)
        if (i + 1) % (2**row) == 0:
            print
        print repr(heap[i]).center(spacing - 1),
    print
    print


def downheap(heap, index):
    """Push the element down the heap until it ends at appropriate position"""
    # Get a slice of one or two element's children. Slicing guarantees we don't
    # go over the array index boundaries
    i = index
    children = heap[2*i+1:2*i+3]
    #print "Element", heap[i],"at", i,", children are", children, "starting from", 2*i+1

    # Determine a maximal number out of two children if there are any
    if len(children) > 0:
        max_child = children[0]
        max_idx = 2*i + 1
        if len(children) == 2:
            if max_child < children[1]:
                max_child = children[1]
                max_idx = 2*i + 2
    else:
        return

    # If a child is larger than parent, exchange them
    if max_child > heap[i]:
        heap[i], heap[max_idx] = heap[max_idx], heap[i]
        #print "exchanging", i, "with", max_idx
        # Element has moved down the heap. Call this to put it further down if needed
        downheap(heap, max_idx)


def construct_heap(list):
    """Construct max-heap in-place, using representation when children
       for i-th element are at 2i+1 and 2i+2 indices"""
    k = len(list) / 2
    # Rightmost part of the list left untouched. Leftmost part is 'downheaped'
    # one by one, thus the heap is being built
    for i in reversed(range(k)):
        downheap(list, i)


def heapsort(list):
    """Heap sort in place"""
    construct_heap(list)
    if __name__ == "__main__":
        display_heap(list)
    for i in reversed(range(len(list))):
        # Swap root element with last one and them
        # interpret tha last one as not being part of the heap and move
        # to the previoous one
        list[i], list[0] = list[0], list[i]
        tmp_list = list[:i]
        #print len(tmp_list), tmp_list, "|", list[i:]
        if len(tmp_list) == 1:
            break
        downheap(tmp_list, 0)
        list[:i] = tmp_list



################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list
    print

    heapsort(list)

    print list


