#! /usr/bin/env python

"""
Insertion sort implemented in python
"""

from list_gen import random_list


################ functions ##################

def insertion_sort_for(list):
    """Insertion sort using for loop without any python list-handling methods"""
    for i in range(1, len(list)):

        # no need to reinsert if elements is larger than previous
        if list[i - 1] <= list[i]:
            continue

        # remember this element for future reassignment
        el = list[i]
        j = 0
        for j in range(i, 0, -1):
            if list[j - 1] > el:
                list[j] = list[j - 1]
            else:
                break
        else:
            j -= 1

        list[j] = el


def insertion_sort_while(list):
    """Insertion sort using while loop and list.insert()"""
    for i in range(1, len(list)):

        j = i - 1
        # no need to reinsert if elements is larger than previous
        if list[j] <= list[i]:
            continue

        # find out index after which to insert value
        while j >= 0 and list[j] > list[i]:
            j -= 1

        # insert it there
        list.insert(j + 1, list.pop(i))


insertion_sort = insertion_sort_for


################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list
    print

    insertion_sort(list)

    print list

