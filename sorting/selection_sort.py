#! /usr/bin/env python

"""
Selection sort implemented in python
"""

from list_gen import random_list


################ functions ##################

def selection_sort(list):
    """Selection sort in place"""
    for i in range(0, len(list) - 1):
        el_min = i
        # look for an element smaller than current
        for j in range(i + 1, len(list)):
            if list[j] < list[el_min]:
                el_min = j
        # if such element is found, exchange with current one
        if el_min != i:
            list[i], list[el_min] = list[el_min], list[i]


################# main #####################

if __name__ == "__main__":
    list = random_list(20)
    print "List to be sorted:"
    print list
    print

    selection_sort(list)

    print list

